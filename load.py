import Tkinter as tk
import myNotebook as nb
import sys, os, plug, re, string, shutil
from config import config

this = sys.modules[__name__]	# For holding module globals

def plugin_prefs(parent, cmdr, is_beta):
    this.DestPath = tk.StringVar(value=config.get("BSS_DestPath"))
    this.SourceFolder = tk.StringVar(value=config.get("BSS_SourceFolder"))

    frame = nb.Frame(parent)
    frame.columnconfigure(2, weight=1)
    PADY = 5

    nb.Label(frame, text="Destination file: ").grid(column=0, row=1, padx=0, pady=(0,PADY+5), sticky=tk.E)
    nb.Entry(frame, textvariable=this.DestPath).grid(column=1, columnspan=3, row=1, padx=0, pady=(0,PADY), sticky=tk.EW)

    nb.Label(frame, text="Source folder: ").grid(column=0, row=3, padx=0, pady=(0,PADY+5), sticky=tk.E)
    nb.Entry(frame, textvariable=this.SourceFolder).grid(column=1, columnspan=3, row=3, padx=0, pady=(0,PADY), sticky=tk.EW)

    nb.Label(frame, text="ShipID = Ship Name").grid(column=0, columnspan=3, row=5, padx=0, pady=(0,PADY), sticky=tk.NW)
    nb.Label(frame, justify=tk.LEFT, text=\
        "adder = Adder\n"
        "anaconda = Anaconda\n"
        "asp = ASP Explorer\n"
        "asp_scout = ASP Scout\n"
        "belugaliner = Beluga Liner\n"
        "typex_3 = Alliance Challenger\n"
        "typex = Alliance Chieftain\n"
        "typex_2 = Alliance Crusader\n"
        "cobramkiii = Cobra MkIII\n"
        "cobramkiv = Cobra MkIV\n"
        "diamondbackxl = Diamondback Explorer\n"
        "diamondback = Diamondback Scout\n"
        "dolphin = Dolphin\n"
        "eagle = Eagle\n"
        "federation_dropship_mkii = Federal Assault Ship\n"
        "federation_corvette = Federal Corvette\n"
        "federation_dropship = Federal Dropship\n"
        "federation_gunship = Federal Gunship\n").grid(column=0, columnspan=2, row=6, padx=0, pady=(0,0), sticky=tk.EW)
    nb.Label(frame, justify=tk.LEFT, text=\
        "ferdelance = Fer-de-Lance\n"
        "hauler = Hauler\n"
        "empire_trader = Imperial Clipper\n"
        "empire_courier = Imperial Courier\n"
        "cutter = Imperial Cutter\n"
        "empire_eagle = Imperial Eagle\n"
        "independant_trader = Keelback\n"
        "krait_mkii = Krait MkII\n"
        "orca = Orca\n"
        "python = Python\n"
        "sidewinder = Sidewinder\n"
        "type6 = Type-6 Transporter\n"
        "type7 = Type-7 Transporter\n"
        "type9 = Type-9 Heavy\n"
        "type9_military = Type-10 Defender\n"
        "viper = Viper MkIII\n"
        "viper_mkiv = Viper MkIV\n"
        "vulture = Vulture").grid(column=2, columnspan=2, row=6, padx=0, pady=(0,0), sticky=tk.EW)
    return frame

def prefs_changed(cmdr, is_beta):
    config.set('BSS_DestPath', this.DestPath.get())
    config.set('BSS_SourceFolder', this.SourceFolder.get())
    copyImage(config.get('BSS_PrevShip'))

def plugin_start():
    if config.get('BSS_DestPath') == None:
        config.set('BSS_DestPath', "")
    if config.get('BSS_SourceFolder') == None:
        config.set('BSS_SourceFolder', "")
    if config.get('BSS_PrevShip') == None or config.get('BSS_PrevShip') == "":
        config.set('BSS_PrevShip', "None")
    copyImage(config.get('BSS_PrevShip'))
    print "BG Ship Switcher is in!"
    return "BG Ship Switcher"

def plugin_stop():
    print "BG Ship Switcher is out!"

def journal_entry(cmdr, system, station, entry, state):
    if entry["event"] == "Loadout":
        BSS_CurrentShip = str(entry["Ship"])
        BSS_CurrentShip = BSS_CurrentShip.lower()
        copyImage(BSS_CurrentShip)

def copyImage(currentShip): #Creates the export files with empty data (not empty really but like if so)

    destinationFile = config.get('BSS_DestPath')
    sourceFolder = config.get('BSS_SourceFolder')

    try:
        os.stat(sourceFolder+"\\"+currentShip+".png")
        shutil.copy(sourceFolder+"\\"+currentShip+".png", destinationFile)
        config.set('BSS_PrevShip', currentShip)
    except Exception as e:
        print("Ship file doesn't exist: "+sourceFolder+"\\"+currentShip+".png")
