# EDMC-Background Ship Switcher
#### Description
Plugin for [ED Market Connector (EDMC)](https://github.com/Marginal/EDMarketConnector), which once set up, will automatically replace a png file with another according with the ship you switched to in Elite Dangerous, so if you like to change your OBS background according to what ship you are flying, you can automate it with this.

**BACKGROUND IMAGES ARE NOT PROVIDED**

#### Installation
On the Plugins settings tab in EDMC press the "Open" button. This reveals the plugins folder where EDMC looks for plugins.

Once there, create a folder for the plugin, "BG Ship Switcher" p.e., and drop the `load.py` file you should have downloaded from this repository, p.e. `\EDMarketConnector\plugins\BG Ship Switcher`.

#### Usage
In the `Destination File` entry box you will paste the path to the file to be replaced, p.e. `D:\StreamResources\Pics\shipbackground.png`

In the `Source Folder` entry box you will paste the path to the folder containing the images that will be copied, p.e. `D:\StreamResources\Pics\SourceShipsFolder`, this folder won't be modified by the plugin, it will just read it.

Under that you will have the name of the files to use, remember, all files must be `.PNG`.

#### Current ship type list
```
        adder = Adder
        anaconda = Anaconda
        asp = ASP Explorer
        asp_scout = ASP Scout
        belugaliner = Beluga Liner
        typex_3 = Alliance Challenger
        typex = Alliance Chieftain
        typex_2 = Alliance Crusader
        cobramkiii = Cobra MkIII
        cobramkiv = Cobra MkIV
        diamondbackxl = Diamondback Explorer
        diamondback = Diamondback Scout
        dolphin = Dolphin
        eagle = Eagle
        federation_dropship_mkii = Federal Assault Ship
        federation_corvette = Federal Corvette
        federation_dropship = Federal Dropship
        federation_gunship = Federal Gunship
        ferdelance = Fer-de-Lance
        hauler = Hauler
        empire_trader = Imperial Clipper
        empire_courier = Imperial Courier
        cutter = Imperial Cutter
        empire_eagle = Imperial Eagle
        independant_trader = Keelback
        krait_mkii = Krait MkII
        orca = Orca
        python = Python
        sidewinder = Sidewinder
        type6 = Type-6 Transporter
        type7 = Type-7 Transporter
        type9 = Type-9 Heavy
        type9_military = Type-10 Defender
        viper = Viper MkIII
        viper_mkiv = Viper MkIV
        vulture = Vulture
```

#### Example (in my case)
![](https://i.imgur.com/pDQIvyt.jpg)

#### Video test
[![Example video](https://img.youtube.com/vi/Y99TCWgRZnM/0.jpg)](https://youtu.be/Y99TCWgRZnM)
